﻿namespace ОКТОКОТTestWork
{
    partial class TestWorkForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.dirPathTextBox = new System.Windows.Forms.TextBox();
            this.choosePathButton = new System.Windows.Forms.Button();
            this.LoadButton = new System.Windows.Forms.Button();
            this.StopButton = new System.Windows.Forms.Button();
            this.loadProgressBar = new System.Windows.Forms.ProgressBar();
            this.workPathBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.SuspendLayout();
            // 
            // dirPathTextBox
            // 
            this.dirPathTextBox.Location = new System.Drawing.Point(13, 13);
            this.dirPathTextBox.Name = "dirPathTextBox";
            this.dirPathTextBox.ReadOnly = true;
            this.dirPathTextBox.Size = new System.Drawing.Size(540, 20);
            this.dirPathTextBox.TabIndex = 0;
            // 
            // choosePathButton
            // 
            this.choosePathButton.Location = new System.Drawing.Point(559, 10);
            this.choosePathButton.Name = "choosePathButton";
            this.choosePathButton.Size = new System.Drawing.Size(26, 23);
            this.choosePathButton.TabIndex = 1;
            this.choosePathButton.Text = "...";
            this.choosePathButton.UseVisualStyleBackColor = true;
            this.choosePathButton.Click += new System.EventHandler(this.choosePathButton_Click);
            // 
            // LoadButton
            // 
            this.LoadButton.Location = new System.Drawing.Point(591, 10);
            this.LoadButton.Name = "LoadButton";
            this.LoadButton.Size = new System.Drawing.Size(75, 23);
            this.LoadButton.TabIndex = 2;
            this.LoadButton.Text = "Load";
            this.LoadButton.UseVisualStyleBackColor = true;
            this.LoadButton.Click += new System.EventHandler(this.LoadButton_Click);
            // 
            // StopButton
            // 
            this.StopButton.Location = new System.Drawing.Point(559, 39);
            this.StopButton.Name = "StopButton";
            this.StopButton.Size = new System.Drawing.Size(107, 23);
            this.StopButton.TabIndex = 3;
            this.StopButton.Text = "Cancel";
            this.StopButton.UseVisualStyleBackColor = true;
            this.StopButton.Click += new System.EventHandler(this.StopButton_Click);
            // 
            // loadProgressBar
            // 
            this.loadProgressBar.Location = new System.Drawing.Point(13, 40);
            this.loadProgressBar.Name = "loadProgressBar";
            this.loadProgressBar.Size = new System.Drawing.Size(540, 23);
            this.loadProgressBar.TabIndex = 4;
            // 
            // TestWorkForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(674, 78);
            this.Controls.Add(this.loadProgressBar);
            this.Controls.Add(this.StopButton);
            this.Controls.Add(this.LoadButton);
            this.Controls.Add(this.choosePathButton);
            this.Controls.Add(this.dirPathTextBox);
            this.Name = "TestWorkForm";
            this.Text = "TestWork";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox dirPathTextBox;
        private System.Windows.Forms.Button choosePathButton;
        private System.Windows.Forms.Button LoadButton;
        private System.Windows.Forms.Button StopButton;
        private System.Windows.Forms.ProgressBar loadProgressBar;
        private System.Windows.Forms.FolderBrowserDialog workPathBrowserDialog;
    }
}

