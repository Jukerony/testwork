﻿using System;
using System.IO;
using System.Windows.Forms;
using ОКТОКОТTestWork.Utility;

namespace ОКТОКОТTestWork
{
    public partial class TestWorkForm : Form
    {
        public TestWorkForm()
        {
            InitializeComponent();
            loadProgressBar.MarqueeAnimationSpeed = 0;
        }

        private void choosePathButton_Click(object sender, EventArgs e)
        {
            DialogResult result = workPathBrowserDialog.ShowDialog();
            var path = workPathBrowserDialog.SelectedPath;
            if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(path))
            {
                if (!Directory.Exists(path))
                {
                    MessageBox.Show("Please select a correct folder", "Warning", MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                    return;
                }
                dirPathTextBox.Text = path;
            }
        }

        private async void LoadButton_Click(object sender, EventArgs e)
        {
            var chosenPath = dirPathTextBox.Text;
            if (!Directory.Exists(chosenPath))
            {
                MessageBox.Show("Please select a correct folder", "Warning", MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                return;
            }
            var files = Directory.GetFiles(chosenPath, "*.txt", SearchOption.AllDirectories);
            loadProgressBar.Maximum = files.Length;
            if (files.Length == 0)
            {
                MessageBox.Show("Folder doesn't contains a .txt files", "Warning", MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                return;
            }
            if (InFileNumbersCalculator.IsWorking())
            {
                MessageBox.Show("Program is already running, please stop it first", "Warning", MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                return;
            }
            IProgress<int> progress = new Progress<int>(value => { loadProgressBar.Value = value; });
            try
            {
                var sum = await InFileNumbersCalculator.Start(files, progress);
                MessageBox.Show($"Result:{sum}", "Result", MessageBoxButtons.OK, MessageBoxIcon.Information);
                progress.Report(0);
            }
            catch (Exception exception)
            {
                MessageBox.Show($"Exception: {exception.Message}", "Exception", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        private void StopButton_Click(object sender, EventArgs e)
        {
            if (InFileNumbersCalculator.Stop())
            {
                MessageBox.Show("Action has successfully stopped", "Notify", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Has no actions for stop", "Notify", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}