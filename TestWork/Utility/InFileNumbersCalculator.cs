﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ОКТОКОТTestWork.Utility
{
    public static class InFileNumbersCalculator
    {
        private static readonly object Locker = new object();
        private static Task _workTask;
        private static CancellationTokenSource _cancelTokenSource;

        /// <summary>
        /// Метод возвращающий сумму целочисленных чисел содержащих в переданном файле. В случае если указанный файл не существует, или не содержит чисел, то будет возвращен 0
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        private static long CalculateInFileNumbers(string path)
        {
            if (!File.Exists(path)) return 0;
            long sum = 0;
            using (var fileReader = new StreamReader(path))
            {
                while (!fileReader.EndOfStream)
                {
                    if (_cancelTokenSource.IsCancellationRequested) break;
                    var readedString = fileReader.ReadLine();
                    if (!string.IsNullOrEmpty(readedString))
                    {
                        int outValue;
                        int.TryParse(readedString.Trim(), out outValue);
                        sum += outValue;
                    }
                }
            }
            return sum;
        }

        /// <summary>
        /// Асинхронный метод, выполняющий сложение целочисленных чисел содержащихся в заданных файлах
        /// </summary>
        /// <param name="files"></param>
        /// <param name="progressCallback"></param>
        /// <returns></returns>
        public static async Task<long> Start(IEnumerable<string> files, IProgress<int> progressCallback)
        {
            var filesCount = files.Count();
            long sum = 0;
            var progressStep = 0;
            if (filesCount == 0) return 0;
            _workTask = new Task(() =>
            {
                using (_cancelTokenSource = new CancellationTokenSource())
                {
                    try
                    {
                        Parallel.ForEach(files,
                            new ParallelOptions()
                            {
                                CancellationToken = _cancelTokenSource.Token,
                                MaxDegreeOfParallelism = Environment.ProcessorCount * 10
                            }, (tempFile, state) =>
                            {
                                var tempSum = CalculateInFileNumbers(tempFile);
                                lock (Locker)
                                {
                                    sum += tempSum;
                                    progressStep += 1;
                                    progressCallback.Report(progressStep);
                                }
                            });
                    }
                    catch (OperationCanceledException)
                    {
                        progressCallback.Report(0); //отмена операции, обнуляем счетчик
                    }
                }
            });
            _workTask.Start();
            await _workTask;
            return sum;
        }

        /// <summary>
        /// Метод остановки работы вычислений по файлам
        /// </summary>
        public static bool Stop()
        {
            try
            {
                if (_cancelTokenSource != null)
                {
                    _cancelTokenSource.Cancel();
                    return true;
                }
            }
            catch (ObjectDisposedException)
            {
                return false; //токен удален
            }
            return false;
        }

        /// <summary>
        /// Проверка на то что в данный момент выполняется вычисление
        /// </summary>
        /// <returns></returns>
        public static bool IsWorking()
        {
            if (_workTask != null && !_workTask.IsCanceled && !_workTask.IsCompleted &&
                !_workTask.IsFaulted) return true;
            return false;
        }
    }
}